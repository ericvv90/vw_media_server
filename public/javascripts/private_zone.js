var  videoSelected="";

    var currentState={
        screen1:{
          video_id:"",
          user_id:""
        },
        screen2:{
          video_id:"",
          user_id:""
        },
        screen3:{
          video_id:"",
          user_id:""
        },
        screen4:{
          video_id:"",
          user_id:""
        },  
        fullScreen:{
          video_id:"",
          user_id:""
        }
      }

  var videoPlaying={
    screen_selected:"",
    user_id:"",
    video_id:""
  }

$(document).ready(function(){

    if(localStorage.getItem("user_id")===null){
        var d=new Date()
        d=d.getTime()
        localStorage.setItem('user_id', d);
    }

    // start socket io connection
    var socket = io();

    // socket.on('updateStatus', function(msg){
    //   alert("updating status")
    // });
    socket.on('playVideo', function(data){
      updateState(data);
    });
    socket.on('resetState', function(){
      resetState();
    });

    socket.on('quitVideo', function(data){

      updateStateFromQuitVideo(data)
      // stop audio if it was me who quit the video

      console.log("executing socket on quitVideo")
    });

    $('.collapsible').collapsible();
  // se tiene que inicializar el modal ... punto menos para materialize
    $('.modal').modal();

    initializePlayButtons();

  	$("#screen1Option").click((event)=>{
      console.log("sending message on socket")
  		var screen_selected= "screen1"
  		selectVideo(videoSelected, screen_selected, socket)
  	})
 	  $("#screen2Option").click((event)=>{
 		var screen_selected= "screen2"
  		selectVideo(videoSelected, screen_selected, socket)
  	})
  	$("#screen3Option").click((event)=>{
  		var screen_selected= "screen3"
  		selectVideo(videoSelected, screen_selected, socket)
  	})
  	$("#screen4Option").click((event)=>{
  		var screen_selected= "screen4"
  		selectVideo(videoSelected, screen_selected, socket)
  	});

    $("#optionFullScreen").click(()=>{
      var screen_selected= "fullScreen"
      selectVideo(videoSelected, screen_selected, socket)
    })

    $("#quitVideo").click(()=>{
      // pendiente:loading
      closeNav()
      socket.emit("quitVideo", videoPlaying);
    })

    getState().then(function(data){
      // when app starts, set screens 
      currentState=data;
      console.log("state was set");
      console.log(currentState);
      onGetState(data);
      //askForState(currentState);
    }).catch(function(err){
      console.log(err)
    });

})


function selectVideo(video_id, screen_selected, socket){
    //playAudio(video_id);
    openNav()

    var audio_id=video_id+"_audio"
    document.getElementById(audio_id).play();
    document.getElementById(audio_id).pause();
  	var data={
  		video_id:video_id,
  		screen_selected:screen_selected,
      user_id: localStorage.getItem("user_id")
  	};
  socket.emit("playVideo", data);

}

function getState(){
  return new Promise (function(resolve,reject){
      $.ajax({ 
         url: "/screen/state",
         method: "get",
           success: function (data) {
              //console.log("data coming from server");
              //console.log(data);
              resolve(data);

          },
          error: function (ajaxContext) {
              reject(ajaxContext.responseText);
          }
      })
  })
}


function checkFullScreenAvailable(state){
  if(areScreensEmpty(state)){
    $("#optionFullScreen").show();
  }else{
     $("#optionFullScreen").hide();
  }

}

function areScreensEmpty(newState){
    var jQuery_each_result=true;
    jQuery.each(newState, function(screen, video) {
      if(video.video_id!=""){
        jQuery_each_result=false;
        return false;
      }
  });
    if(jQuery_each_result){
      return true;
    }else{
      return false;
    }
}

function playAudio(video_id){
      //$("#audio1").play();
      var audio_id=video_id+"_audio"
      //document.getElementById(audio_id)currentTime=0;
      document.getElementById(audio_id).play(); 
      // setTimeout(function(){
      // document.getElementById(audio_id).play();        
      // },500)
}

function initializePlayButtons(){

    $("#selectVideo1").click(()=>{
      videoSelected="video1";
      $("#selectScreenModal").modal('open');
    })

    $("#selectVideo2").click(()=>{
      videoSelected="video2";
      $("#selectScreenModal").modal('open');
    })

    $("#selectVideo3").click(()=>{
      videoSelected="video3";
      $("#selectScreenModal").modal('open');
    })

    $("#selectVideo4").click(()=>{
      videoSelected="video4";
      $("#selectScreenModal").modal('open');
    })
};

function updateState(data){

  // update state
  currentState[data.screen_selected].user_id = data.user_id;
  currentState[data.screen_selected].video_id = data.video_id;

  console.log("current state in updateState");
  console.log(currentState)

  // play audio if it was me
  if(data.user_id===localStorage.getItem("user_id")){
    if(data.screen_selected!=="fullScreen"){
           playAudio(data.video_id)
      $("#turnUpVolume").show()    
    }else{

        $("#turnUpVolume").hide()
    }
    $("#screenPlayingVideo").html(data.screen_selected)
      videoPlaying.user_id=data.user_id
      videoPlaying.screen_selected=data.screen_selected
      videoPlaying.video_id=data.video_id
  }

  // hide, if necesary, full screen video
  checkFullScreenAvailable(currentState);
  updatePlayButtons(currentState)
}

function updateStateFromQuitVideo(data){
  // update state
  // play audio if it was me
  console.log("executing updateStateFromQuitVideo")
  //alert(localStorage.getItem("user_id")+" "+data.user_id);
  if(data.user_id===localStorage.getItem("user_id")){
    if(data.screen_selected!=="fullScreen"){
     stopAudio(data.video_id)
     videoPlaying.user_id=data.user_id
     videoPlaying.screen_selected=data.screen_selected
     videoPlaying.video_id=data.video_id
    }
  }
  currentState[data.screen_selected].user_id = "";
  currentState[data.screen_selected].video_id = "";
  // hide, if necesary, full screen video
  checkFullScreenAvailable(currentState);
  updatePlayButtons(currentState)
}

function updatePlayButtons(state){
  console.log("executing updatePlayButtons");
  if(state.fullScreen.video_id!==""){
    $(".playBtn").hide()
    return
  }
  if(areScreensEmpty(state)){
    $(".playBtn").show()
    return
  }

  jQuery.each(state, function(screen, video) {
      if(video.video_id!==""){
        //var playBtnId= replace('stringToReplace','');
        console.log(video.video_id);
        $("#"+screen+"Option").hide();
      }else{
        $("#"+screen+"Option").show();
      }
  });
}

function onGetState(currentState){
    // hide, if necesary, full screen video
    checkFullScreenAvailable(currentState);
    updatePlayButtons(currentState)
}

function resetState(){
  currentState={
          screen1:{
            video_id:"",
            user_id:""
          },
          screen2:{
            video_id:"",
            user_id:""
          },
          screen3:{
            video_id:"",
            user_id:""
          },
          screen4:{
            video_id:"",
            user_id:""
          },  
          fullScreen:{
            video_id:"",
            user_id:""
          }
  }

  videoPlaying={
    screen:"",
    user_id:"",
    video_id:""
  }
    checkFullScreenAvailable(currentState);
    updatePlayButtons(currentState)
    //if any
    stopAllAudios()

}

function stopAllAudios(){
      $('.audio').each(function(){
          this.pause(); // Stop playing
          this.currentTime = 0; // Reset time
      }); 
}


function openNav() {
    document.getElementById("myNav").style.width = "100%";
}

function closeNav() {
    document.getElementById("myNav").style.width = "0%";
}

function stopAudio(video_id){
       stopAllAudios()
      // var audio_id=video_id+"_audio"
      // console.log(audio_id)
      // document.getElementById(audio_id).stop(); 
}