$(document).ready(function(){
	$('.modal').modal();
	 var socket = io();


	$("#fullScreen").hide();

		var currentState={
	        screen1:{
	          video_id:"",
	          user_id:""
	        },
	        screen2:{
	          video_id:"",
	          user_id:""
	        },
	        screen3:{
	          video_id:"",
	          user_id:""
	        },
	        screen4:{
	          video_id:"",
	          user_id:""
	        },  
	        fullScreen:{
	          video_id:"",
	          user_id:""
	        }
	}

	$("#btnPlay").click(function(){
		document.getElementById("video1").play();
		document.getElementById("video2").play();
		document.getElementById("video3").play();
		document.getElementById("video4").play();
	});

	$("#btnStop").click(function(){
		document.getElementById("video1").pause();
		document.getElementById("video2").pause();
		document.getElementById("video3").pause();
		document.getElementById("video4").pause();
	})

	$("#fullScreen").click(function(){
		$(document).toggleFullScreen();
	})

	getState().then(function(data){
		// when app starts, set screens 
		updateScreens(currentState, data);
		currentState=data;
		console.log("state was set");
		console.log(currentState);
		//askForState(currentState);
		}).catch(function(err){
			console.log(err)
		});


	socket.on('playVideo', function(data){
		console.log("the state has changed");
		console.log(data);
		//updateScreens(currentState, data);
		updateScreenSocket(data);
	});
	socket.on('resetState', function(){
		cleanScreens();
	});

	socket.on('quitVideo', function(data){
		quitVideo(data);
	});



	$(document).keypress(function(e) {
		console.log(e.which);
		if(e.which == 102) {
			$(document).toggleFullScreen();
		}else if(e.which == 49){
			changeToFullScreen();
		}else if(e.which == 52){
			$("#fourScreensContainer").show();
			changeTo4Screens();
		}else if(e.which ===111){
			$("#optionsModal").modal('open');
		}
	});

	$("#optionFullScreen").click(function(){
		changeToFullScreen();
	})

	$("#option4Screen").click(function(){
		changeTo4Screens();
	})

	$("#optionClearAll").click(function(){
		clearAll(socket);
	});
	$("#optionStopAll").click(function(){

		if(document.getElementById("video1") != null){
			document.getElementById("video1").pause();	
		}

		if(document.getElementById("video2") != null){
			document.getElementById("video2").pause();	
		}

		if(document.getElementById("video3") != null){
			document.getElementById("video3").pause();	
		}

		if(document.getElementById("video4") != null){
			document.getElementById("video4").pause();	
		}

	});

	$("#optionResumeAll").click(function(){
		if(document.getElementById("video1") != null){
			document.getElementById("video1").play();	
		}

		if(document.getElementById("video2") != null){
			document.getElementById("video2").play();	
		}

		if(document.getElementById("video3") != null){
			document.getElementById("video3").play();	
		}

		if(document.getElementById("video4") != null){
			document.getElementById("video4").play();	
		}
	});

	$("#optionFullScreenMode").click(function(){
		$(document).toggleFullScreen();
	})
	//setVideoOnScreen("video1", "div1")
})// end ready document


function askForState(currentState){
	getState().then(function(newState){
		// check if state has change
		if(hasChagedState(currentState, newState)){ // has changed
        	console.log("the state has changed");
        	//update state
        	updateScreens(currentState, newState);
        	currentState = newState;
        	// do changes in screens;

		}else{ // nothing has changed
         	//console.log("the state has not changed");
         	//update state
         	currentState = newState;
		};
			
		// ask again for the state
			setTimeout(function(){
				//console.log("asking for state")
				askForState(currentState);
			}, 500);
		}).catch(function(err){
			console.log(err)
		});


}

function getState(){
	return new Promise (function(resolve,reject){
			$.ajax({ 
	 		   url: "/screen/state",
	 		   method: "get",
	    	   success: function (data) {
	    	   		//console.log("data coming from server");
	    	   		//console.log(data);
	        		resolve(data);

	    		},
	    		error: function (ajaxContext) {
	        		reject(ajaxContext.responseText);
	   			}
			})
	})
}


function hasChagedState(currentState, newState){
	//console.log("currentState in hasChanged");
	//console.log(currentState);
		var currentStateString = JSON.stringify(currentState);
		var newStateString =  JSON.stringify(newState);
			//console.log("comparing objects");
			//console.log(currentStateString);
			//console.log("currentState in hasChanged");
			//console.log(currentState);
		if(currentStateString === newStateString){
				return false;
		}else{
				return true;
		}

}

function updateScreens(currentState, newState){
//   	console.log("-----------> current state");
//   	//console.log(currentState);
//   	jQuery.each(currentState, function(i, val) {
	//  	console.log("i: "+i);
	//  	console.log("val: "+val.video_id);
	// });
	// console.log("-----------> newState");
	// cambiar esto: actualizar solo el que sea necesario
	jQuery.each(newState, function(screen, video) {
		if(currentState[screen].video_id != video.video_id){
			if(video.video_id===""){

				if(screen==="fullScreen"){
					$("#"+screen).empty();
					  var html=    "<br>"+
					      "<br>"+
					      "<br>"+
					      "<br>"+
					      "<br>"+
					      '<h1 style="color:white"> FULL SCREEN VIDEO</h1>'
					   $("#"+screen).append(html);
				}else{

					$("#"+screen).empty();
					var html="<br>" 
	    			+"<br>"
	    			+"<h1>"+screen+"</h1>";
	    			$("#"+screen).append(html);		
				}


			}else{
				setVideoOnScreen(video.video_id, screen);
				if(screen==="fullScreen"){
					changeToFullScreen();
				}else{
					changeTo4Screens();
				}
			}

		}
	});

	// jQuery.each(newState, function(i, val) {
	// 	console.log("i: "+i);
	//  	console.log("val: "+val.video_id);
	// 	if(currentState[i].video_id != val.video_id){
	// 		setVideoOnScreen(val.video_id, i);
	// 		if(){

	// 		}
	// 	}
	// });

	// pendiente verificar que no exista un video corriendo en ese screen
}

function setVideoOnScreen(video_id, screen_id){
	var muted="muted"
	if(screen_id==="fullScreen"){
		muted="";
	}
	var video_text=		'<video controls autoplay id="'+video_id+'""  width="100%" height="100%" controls '+muted+'>'+
	  '<source src="./videos/HD/'+video_id+'.mp4" type="video/mp4">'+
	  'Your browser does not support the video tag.'+
	   '</video>' 
	$("#"+screen_id).empty();
	$("#"+screen_id).append(video_text);    		
}

function hasChangedVideo(){

}

function changeToFullScreen(){
	$("#fourScreensContainer").hide();
	$("#fullScreen").show();
}

function changeTo4Screens(){
	$("#fourScreensContainer").show();
	$("#fullScreen").hide();
}

function clearAll(socket){
  socket.emit("resetState", "no data needed");

	// $.ajax({
	//   type: "POST",
	//   url: "/clearAll",
	// }).done((res)=>{
	// 	console.log(res);
	// })

}

function updateScreenSocket(data){
	console.log(data.screen_selected)
	setVideoOnScreen(data.video_id, data.screen_selected)
	if(data.screen_selected==="fullScreen"){
		changeToFullScreen();
	}else{
		changeTo4Screens();
	}
}

function cleanScreens(){
		var currentState={
	        screen1:{
	          video_id:"",
	          user_id:""
	        },
	        screen2:{
	          video_id:"",
	          user_id:""
	        },
	        screen3:{
	          video_id:"",
	          user_id:""
	        },
	        screen4:{
	          video_id:"",
	          user_id:""
	        },  
	        fullScreen:{
	          video_id:"",
	          user_id:""
	        }
	}

	jQuery.each(currentState, function(screen, video) {
			$("#"+screen).empty();
			var html="<br>" 
			+"<br>"
			+"<h1>"+screen+"</h1>";
			$("#"+screen).append(html);		

		})
}

function quitVideo(data){

	if(data.screen_selected==="fullScreen"){
		$("#"+data.screen_selected).empty();
		  var html=    "<br>"+
		      "<br>"+
		      "<br>"+
		      "<br>"+
		      "<br>"+
		      '<h1 style="color:white"> FULL SCREEN VIDEO</h1>'
		   $("#"+data.screen_selected).append(html);
		   changeTo4Screens();
	}else{
		$("#"+data.screen_selected).empty();
		var html="<br>" 
		+"<br>"
		+"<h1>"+data.screen_selected+"</h1>";
		$("#"+data.screen_selected).append(html);		
	}

	// $("#"+data.screen_selected).empty();
	// var html="<br>" 
	// +"<br>"
	// +"<h1>"+data.screen_selected+"</h1>";
	// $("#"+data.screen_selected).append(html);		
}